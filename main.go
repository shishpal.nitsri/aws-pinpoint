package main

import (
"fmt"
"github.com/aws/aws-sdk-go/aws"
"github.com/aws/aws-sdk-go/aws/session"
"github.com/aws/aws-sdk-go/service/pinpoint"
)

var (
	region            = "us-west-2"
	originationNumber = "+12065550199"
	destinationNumber = "+916005223173"
	message           = "Amazon pinpoint welcomes you!!!"
	applicationId     = ""
	messageType       = "TRANSACTIONAL"
	registeredKeyword = "myKeyword"
	senderId          = "MySenderID"
)

func main() {
	mySession := session.Must(session.NewSession())

	// Create a Pinpoint client with additional configuration
	pinPointClient := pinpoint.New(mySession, aws.NewConfig().WithRegion("us-west-2"))

	sendMessageInput := prepareSendMessageInput()

	//Send Message using pinPointClient
	out, err := pinPointClient.SendMessages(&sendMessageInput)

	if err != nil {
		fmt.Println("error: ", err)
	}
	fmt.Println(out)
}

func prepareSendMessageInput() pinpoint.SendMessagesInput {
	smsMessage := pinpoint.SMSMessage{
		Body:              &message,
		Keyword:           &registeredKeyword,
		MessageType:       &messageType,
		OriginationNumber: &originationNumber,
		SenderId:          &senderId,
	}

	directMessageConf := &pinpoint.DirectMessageConfiguration{
		SMSMessage: &smsMessage,
	}

	sms := "SMS"
	addressConf := &pinpoint.AddressConfiguration{
		ChannelType: &sms,
	}
	var addr map[string]*pinpoint.AddressConfiguration
	addr["destinationNumber"] = addressConf

	messageReq := &pinpoint.MessageRequest{
		Addresses:            addr,
		MessageConfiguration: directMessageConf,
	}

	sendMessageInput := pinpoint.SendMessagesInput{
		ApplicationId:  &applicationId,
		MessageRequest: messageReq,
	}

	return sendMessageInput
}



